package com.mulcam.schedule.controller;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

@Controller
public class HelloController {
  @Scheduled(cron = "0/5 * * * * *") // cron으로도 설정 가능하다.
  public void scheduledProcess() {
    System.out.println("스케쥴된 작업");
  }
}
