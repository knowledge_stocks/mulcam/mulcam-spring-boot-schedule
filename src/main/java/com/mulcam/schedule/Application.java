package com.mulcam.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling // 보통 Config에 갖다 붙인다. 안 쓰는 Config 만들기 싫어서 application에다가 붙임
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
